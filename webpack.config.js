var webpack = require('webpack');
var path = require("path");
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
    entry: ['./app/app.js'],
    output: {
        path: path.join(__dirname, "mock-server/public"),
        filename: 'dashboard.js'
    },
    plugins: [
        new ExtractTextPlugin("styles.css")
    ],
    module: {
        loaders: [
            {
                test: /\.html$/,
                loader: "html" },
            {
                test:   /\.styl$/,
                loader: ExtractTextPlugin.extract('css!stylus?resolve url')
            },
            {
                test:   /\.jade$/,
                loader: "jade"
            },
            {
                test:   /\.(css|png|jpg|svg|ttf|eot|woff|woff2)$/,
                loader: 'file?name=[path][name].[ext]'
            }
        ]
    }
};