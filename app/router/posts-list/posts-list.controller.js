var angular = require('angular');

angular
    .module('app')
    .controller('PostsListController', PostsListController);

PostsListController.inject = ['$http', '$sce', 'postsService'];

function PostsListController ($stateParams, $sce, postsService) {
    var vm = this;
    var page = $stateParams.page || 1;

    postsService
        .getPostsByPage(page)
        .then(function (posts) {
            vm.posts = posts.map(function (post) {
                post.body = $sce.trustAsHtml(post.body);
                return post;
            });
        });
}