var angular = require('angular');

angular
    .module('app')
    .controller('ChangePostController', ChangePostController);

ChangePostController.$inject = ['$state','postsService', '$stateParams'];
function ChangePostController ($state, postsService, $stateParams) {
    var vm = this;
    var id = $stateParams.postId;

    vm.post = {
        header: '',
        body: ''
    };

    vm.update = update;

    postsService.loadPost(id)
        .then(function (post) {
            if (!post) {
                $state.go('postsNew');
                return;
            }
            vm.post = post;
        });

    function update () {
        postsService
            .update(vm.post)
            .then(function (post) {

            });
    }
}