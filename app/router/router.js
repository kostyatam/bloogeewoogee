var angular = require('angular');

angular
    .module('app')
    .config(router);

router.$inject = ['$stateProvider', '$urlRouterProvider'];

function router ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('page', {
            abstract: true,
            controller: 'PageController',
            controllerAs: 'pageModel',
            template: require('./page/page.html')
        })
        .state('postsNew', {
            parent: 'page',
            url: '/post/',
            template: require('./new-post/index.html'),
            controller: 'NewPostController',
            controllerAs: 'newPostModel'
        })
        .state('postsChange', {
            parent: 'page',
            url: '/post/:postId',
            template: require('./change-post/index.html'),
            controller: 'ChangePostController',
            controllerAs: 'changePostModel'
        })
        .state('postsList', {
            parent: 'page',
            url: '/list',
            template: require('./posts-list/index.html'),
            controller: 'PostsListController',
            controllerAs: 'postsListModel'
        });
    $urlRouterProvider.otherwise(function ($injector) {
        var $state = $injector.get('$state');
        $state.go('postsList');
    })
}