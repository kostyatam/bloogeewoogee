var angular = require('angular');

angular
    .module('app')
    .controller('NewPostController', NewPostController);

NewPostController.$inject = ['$state','postsService', '$stateParams', '$q'];
function NewPostController ($state, postsService) {
    var vm = this;
    vm.post = {
        header: '',
        body: ''
    };

    vm.create = create;

    function create () {
        postsService
            .create(vm.post)
            .then(function (post) {
                $state.go('postsChange', {
                    postId: post._id
                });
            });
    }
}