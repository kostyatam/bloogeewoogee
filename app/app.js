var angular = require('angular');
var uiRouter = require('angular-ui-router');
/*adding main css*/
require('./styles/main.styl');
/**/
angular
    .module('app', [uiRouter]);

require('./services');
require('./router');
require('./components');