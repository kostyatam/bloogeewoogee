angular = require('angular');

angular
    .module('app')
    .service('postsService', PostsService);

PostsService.$inject = ['$http', '$q'];

function PostsService ($http, $q) {
    var postsLimit;
    this.update = update;
    this.create = create;
    this.getPostsByPage = getPostsByPage;
    this.getPosts = getPosts;
    this.getPostById = getPostById;
    this.loadPost = loadPost;

    function update (post) {
        var req = $http({
            method: 'put',
            url: 'http://localhost:3000/posts',
            data: post
        });
        req.catch(errorHandler);

        return req.then(function (res) {
            return res.data.post;
        });

        function errorHandler (err) {
            console.log('handler');
        }
    };

    function create (post) {
        var req = $http({
            method: 'post',
            url: 'http://localhost:3000/posts',
            data: post
        });
        req.catch(errorHandler);

        return req.then(function (res) {
            return res.data.post
        });

        function errorHandler (err) {
            console.log('handler')
        }
    }

    function getPostsLimit () {
        return postsLimit || 5;
    }
    /**
     * @param {limit, offset}
     * or
     * @param {id}
     return Promise
     */
    function getPosts (params) {
        var list = $http({
            method: 'get',
            url: 'http://localhost:3000/posts',
            params: params
        });

        list.catch(function (err) {
            throw new Error('server error')
        });

        return list;
    }

    function getPostsByPage (page) {
        var limit = getPostsLimit();
        var offset = (page - 1) * limit;

        return getPosts({
            limit: limit,
            offset: offset
        }).then(function (res) {
            return res.data.posts;
        })
    }

    function getPostById (id) {
        return getPosts({
            _id: id
        }).then(function (res) {
            return res.data.post[0];
        })
    }

    function loadPost (id) {
        var deferred = $q.defer();

        if (!id && id !== 0) {
            deferred.resolve(false);
            return deferred.promise;
        };

        this.getPostById(id).then(function (post) {
            if (!post) {
                deferred.resolve(false);
            };
            deferred.resolve(post);
        });
        return deferred.promise;
    }
}