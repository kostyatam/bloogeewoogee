var angular = require('angular');
require('./main-menu.styl');
angular
    .module('app')
    .directive('mainMenu', mainMenu);
function mainMenu () {
    var directive = {
        template: require('./main-menu.html'),
        replace: true,
        restrict: 'EA',
        scope: {

        },
        link: linkFunc,
        controller: mainMenuController
    };

    return directive;

    function linkFunc (scope, el, attr, vm) {
    }

    mainMenuController.inject = ['postsService'];

    function mainMenuController () {
        var vm = this;
        this.menu = [{
            title: 'Список',
            state: 'postsList'
        }, {
            title: 'Новый пост',
            state: 'postsNew'
        }];
    }
}