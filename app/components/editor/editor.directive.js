var angular = require('angular');

require('./editor.styl');

var MediumEditor = require('medium-editor');
var TooltipExt = require('../../extensions/medium-editor/tooltip/tooltip');
angular
    .module('app')
    .directive('editor', editor);

function editor () {
    var directive = {
        template: require('./editor.html'),
        replace: true,
        restrict: 'EA',
        scope: {
            model: '='
        },
        link: linkFunc,
        controller: editorController,
        controllerAs: 'editorModel'
    };

    return directive;

    function linkFunc (scope, el, attr, vm) {
        var el = el[0];
        var editorEl = el.getElementsByClassName('editor__body')[0];
        var toolbarEl = el.getElementsByClassName('editor__toolbar')[0];
        var editor = new MediumEditor(editorEl, {
            toolbar: {
                relativeContainer: toolbarEl
            },
            extensions: {
                'tooltip': new TooltipExt
            }
        });
    }

    editorController.inject = ['postsService'];

    function editorController () {
        var vm = this;
    }
}