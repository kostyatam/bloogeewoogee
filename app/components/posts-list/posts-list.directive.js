'use strict'

var angular = require('angular');
require('./posts-list.styl');
angular
    .module('app')
    .directive('postsList', postsList);

function postsList () {
    var directive = {
        template: require('./posts-list.html'),
        replace: true,
        restrict: 'EA',
        scope: {
            posts: '='
        },
        link: linkFunc
    };

    return directive;

    function linkFunc (scope, el, attr, vm) {

    }
}