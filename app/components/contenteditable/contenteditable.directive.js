angular
    .module('app')
    .directive("edit", function() {
    return {
        restrict: "A",
        require: "ngModel",
        link: function(scope, element, attrs, ngModel) {
            var el = element[0];

            function read() {
                ngModel.$setViewValue(el.innerHTML);
            }

            ngModel.$render = function() {
                var event = new Event('input');

                el.innerHTML = ngModel.$viewValue || "";
                el.dispatchEvent(event);
            };

            element.bind("blur keyup change", function() {
                scope.$apply(read);
            });
        }
    };
});