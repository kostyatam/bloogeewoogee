'use strict'

var MediumEditor = require('medium-editor');
require('./tooltip.styl')
var tooltip = MediumEditor.Extension.extend({
    name: 'tooltip',
    init: function () {
        var editor = this.getEditorElements();
        var tooltipTemplate = require('./tooltip.jade');

        editor.map(function (editor) {
            var tooltipEl = document.createElement('div');
            tooltipEl.innerHTML = tooltipTemplate();
            tooltipEl.style.display = 'none';
            tooltipEl.classList.add('tooltip');
            editor.parentNode.classList.add('editor-with-tooltip');
            editor.parentNode.appendChild(tooltipEl);

            var addPicture = document.createElement('li');
            var addPictureInput = document.createElement('input');
            addPictureInput.style.display = 'none';
            addPictureInput.setAttribute('type', 'file');
            addPictureInput.addEventListener('change', function (e) {
                var file = e.target.files[0];
                upload(file);

                function upload(file) {
                    var xhr = new XMLHttpRequest;
                    var formData = new FormData;
                    xhr.upload.onprogress = function(event) {
                        console.log(event.loaded + ' / ' + event.total);
                    };
                    xhr.onload = xhr.onerror = function() {
                        if (this.status == 200) {
                            console.log("success");
                        } else {
                            console.log("error " + this.status);
                        }
                    };
                    xhr.open("POST", "http://localhost:3000/upload/img", true);
                    formData.append(file.name, file);
                    xhr.send(formData);
                };
            });
            addPicture.textContent = 'image';
            addPicture.classList.add('tooltip-list__item');

            tooltipEl
                .getElementsByClassName('tooltip-list')[0]
                .appendChild(addPicture);

            addPicture.addEventListener('click', function () {
                addPictureInput.click();
                that.paragraph.innerHTML =
                    '<div contenteditable="false">' +
                    '<img src="http://blog.marketwired.com/wp-content/uploads/2014/12/MediumLogo_B-W-500x300.jpg"/>' +
                    '<div contenteditable="true">capture</div>' +
                    '</div>';
            });

            var that = this;
            this.on(editor, 'click', newParagraphHandler);
            this.on(editor, 'keyup', newParagraphHandler);
            this.subscribe('showTooltip', showTooltiphandler);
            this.subscribe('hideTooltip', hideTooltiphandler);

            function hideTooltiphandler () {
                tooltipEl.style.display = 'none';
            }

            function showTooltiphandler (paragraph) {
                var position = getPosition(paragraph);
                tooltipEl.style.top = position.top + 'px';
                tooltipEl.style.display = 'block';
                that.paragraph = paragraph;

                function getPosition (elem) {
                    var box = elem.getBoundingClientRect();

                    return {
                        top: box.top + pageYOffset,
                        left: box.left + pageXOffset
                    };
                }
            }

            function newParagraphHandler () {
                var editor = that;
                var selection = window.getSelection();
                var paragraph = getParagraphElement(selection);
                if (!paragraph) {
                    editor.trigger('hideTooltip');
                    return;
                }
                if (isEmpty(paragraph)) {
                    editor.trigger('showTooltip', paragraph);
                    return;
                } else {
                    editor.trigger('hideTooltip');
                    return;
                };

                function isEmpty (p) {
                    if (p.innerHTML === '' || p.innerHTML === '<br>') {
                        return true;
                    }
                }
                function getParagraphElement (selection) {
                    var paragraph;
                    paragraph = selection.parentNode;
                    if (paragraph && paragraph.tagName === 'P') {
                        return paragraph;
                    }
                    paragraph = selection.baseNode;
                    if (paragraph && paragraph.tagName === 'P') {
                        return paragraph;
                    }
                    paragraph = selection.anchorNode;
                    if (paragraph && paragraph.tagName === 'P') {
                        return paragraph;
                    }
                }
            }
        }, this);
    }
});

module.exports = tooltip;

