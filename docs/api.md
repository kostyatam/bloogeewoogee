#posts
##get one post

method: GET

params: {
    _id
}

json: {
    post: [{
        _id,
        header,
        body
    }]
}

##get posts list

method: GET

###request

params: {
    limit,
    offset
}

###response

json: {
    posts: [{
        _id,
        header,
        body
    }]
}

###create post

method: POST

###request

json: {
    _id,
    header,
    body
}

###response


json: {
    _id,
    header,
    body
}
