'use strict'
const Router = require('koa-router');
let api = new Router;

let posts = require('./api.posts');
let signin = require('./api.signin');
let upload = require('./api.upload')
api
    .use(posts.routes(), posts.allowedMethods())
    .use(signin.routes(), signin.allowedMethods())
    .use(upload.routes(), upload.allowedMethods());

module.exports = api;