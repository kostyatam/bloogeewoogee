'use strict'

const signin = require('koa-router')();
const mongoose = require('mongoose');

let User = require('../models/User').User;
let AuthError = require('../models/User').AuthError;
signin.post('/signin', function * () {
    let body = this.request.body;
    let name = body.name;
    let password = body.password;
    let user;

    if (!name || !password) {
        throw(401);
        return;
    }
    try {
        user =yield* User.authorize(name, password);
    } catch (e) {
        if (e instanceof AuthError) {
            this.body = {
                auth: false,
                errorMessage: 'Неверный пароль'
            };
            this.status = 401;
            return;
        };
        throw(500);
        return;
    }

    this.body = {
        auth: true,
        url: '/admin'
    };
    this.status = 200;
    this.session.auth = true;
    this.session.userId = user._id;
});

module.exports = signin;
