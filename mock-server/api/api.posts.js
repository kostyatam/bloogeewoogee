'use strict'

const posts = require('koa-router')();
let Post = require('../models/Post').Post;
let User = require('../models/User').User;
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

posts.get("/posts", function* () {
    var params = this.query;
    if (params.hasOwnProperty('_id')) {
        let id = new ObjectId(params._id);
        let post = yield Post.find({_id: id});

        this.body = {
            post
        };
        return;
    };
    if (params.hasOwnProperty('limit') && params.hasOwnProperty('offset')) {
        let limit = params.limit;
        let offset = params.offset;
        let posts = yield Post.find().skip(offset).limit(limit);
        this.body = {
            posts
        };
        return;
    }
    throw(500);
});

posts.post("/posts", function* () {
    let post = this.request.body;
    let dbPost = yield Post.create(post);
    this.body = {
        post: dbPost
    };
    this.status = 201;
    return;
});

posts.put("/posts", function* () {
    let post = this.request.body;
    if (!post.hasOwnProperty('_id')) {
        throw(400)
        return;
    }
    let id = new ObjectId(post._id);
    let dbPost = yield Post.findByIdAndUpdate({
        _id: id
    }, {
        $set: post
    });
    dbPost = yield dbPost.save();
    this.body = {
        post: dbPost
    };
    this.status = 200;
});

module.exports = posts;