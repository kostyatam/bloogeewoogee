'use strict'

const signin = require('koa-router')();
const mongoose = require('mongoose');

let User = require('../models/User').User;
let AuthError = require('../models/User').AuthError;
signin.post('/signout', function * () {
    if (!this.session.auth) {
        this.status = 401;
        this.body = {
            errorMessage: 'Unauthorized request, already sign out'
        };
        return;
    }
    this.status = 200;
    this.session.destroy();
});

module.exports = signin;
