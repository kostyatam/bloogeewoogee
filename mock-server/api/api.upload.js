'use strict'

const upload = require('koa-router')();
const mongoose = require('mongoose');
let parse = require('co-busboy');
let fs = require('fs');
let path = require('path');
let url = require('url');

upload.post('/upload/img', function * () {
    if (!this.request.is('multipart/*')) {
        throw('400');
        return;
    };

    let parts = parse(this, {
        autoFields: true,
        limits: {
          fileSize: 5e+6
        },
        checkFile: function (fieldname, file, filename) {
            let ext = path.extname(filename);
            if (ext !== '.jpg' && ext !== '.png') {
                throw(400);
                return;
            };
        }
    });
    let that = this;
    let links = {};
    let files = yield* getFiles(parts);
    files = yield files;

    files.map(function (file) {
        links[file.filename] = file.src;
    });
    console.log('links', links)
    this.body = links;

    function* getFiles (parts) {
        let part;
        let hostname = url.parse(that.url).hostname;
        let files = [];
        while (part = yield parts) {
            let file = new Promise(function (resolve, reject) {
                let dest = path.join(__dirname, '../public/posts/image', part.filename);
                let stream = fs.createWriteStream(path.join(dest), {
                    flags: 'w'
                });
                part.pipe(stream);
                part.on('end', function () {
                    var link = {};
                    link.filename = part.filename;
                    link.src = [hostname, part.filename].join('/');
                    resolve(link)
                });
            });
            files.push(file);
        };
        return files;
    }
});

module.exports = upload;
