'use strict'

module.exports = function* (next) {
    try {
        yield* next;
    } catch (e) {
        if (e.status) { // User error
            this.body = e.message;
            this.status = e.status;
        } else { // Server error
            this.body = "Error 500";
            this.status = 500;
            console.error(e.message, e.stack);
        }
    }

}