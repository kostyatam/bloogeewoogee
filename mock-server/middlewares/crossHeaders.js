'use strict'

module.exports = function* (next) {
    this.set("Access-Control-Allow-Origin", "*");
    this.set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    yield* next;
}