var serve = require('koa-static');
var path = require('path');

module.exports = serve(path.resolve(__dirname,'../public'));