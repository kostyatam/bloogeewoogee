'use strict'

let session = require('koa-generic-session');
let mongoStore = require('koa-generic-session-mongo');
module.exports = session({
    store: new mongoStore({
        url: 'mongodb://localhost/test'
    }),
    cookie: {
        path: '/',
        httpOnly: true,
        maxage: null
    }
})