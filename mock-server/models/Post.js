'use strict'

const mongoose = require('mongoose');

let postSchema = new mongoose.Schema({
    header:  String,
    author: String,
    body:   String,
    date: { type: Date, default: Date.now }
});

module.exports.Post = mongoose.model('Post', postSchema);