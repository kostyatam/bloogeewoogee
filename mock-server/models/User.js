'use strict'

const mongoose = require('mongoose');
const crypto = require('crypto');

let util = require('util');
let userSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

userSchema.methods.encryptPassword = function(password) {
    return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
};

userSchema.virtual('password')
    .set(function(password) {
        this._plainPassword = password;
        this.salt = Math.random() + '';
        this.hashedPassword = this.encryptPassword(password);
    })
    .get(function() { return this._plainPassword; });


userSchema.methods.checkPassword = function(password) {
    return this.encryptPassword(password) === this.hashedPassword;
};

userSchema.statics.authorize = function* (username, password) {
    var User = this;

    var user = yield User.findOne({username: username});
    if (user) {
        if (user.checkPassword(password)) {
            return user;
        } else {
            throw AuthError("Пароль неверен");
            return;
        }
    }
    var user = yield User.create({
        username: username,
        password: password
    });
    return user;
};

module.exports.User = mongoose.model('User', userSchema);

function AuthError(message) {
    Error.apply(this, arguments);
    Error.captureStackTrace(this, AuthError);

    this.message = message;
}

util.inherits(AuthError, Error);

AuthError.prototype.name = 'AuthError';

exports.AuthError = AuthError;