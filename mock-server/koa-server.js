'use strict'

const koa = require('koa');
const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/test');

let app = koa();
let api = require('./api/api');
let admin = require('./routes/admin');


let bodyParser = require('koa-bodyparser');
let crossHeaders =  require('./middlewares/crossHeaders');
let errorHandler =  require('./middlewares/errorHandler');
let session = require('./middlewares/session');
let serve = require('./middlewares/static');

app.keys = ['keys', 'keykeys'];

app
    .use(bodyParser())
    .use(errorHandler)
    .use(crossHeaders)
    .use(session)
    .use(serve);

app
    .use(api.routes())
    .use(admin.routes())

app.listen(3000);

console.log('listening port 3000')
