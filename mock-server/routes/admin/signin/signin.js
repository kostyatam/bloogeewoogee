'use strict'
let signin = require('koa-router')();
let jade = require('jade');
let tmpl = jade.compileFile(__dirname + '/signin.jade')();

signin.get('/admin/signin', function * () {
    if (this.session.auth) {
        this.redirect('/admin');
        return;
    }
    this.body = tmpl;
});

module.exports = signin;