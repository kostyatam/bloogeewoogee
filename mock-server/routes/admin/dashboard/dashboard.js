'use strict'
let dashboard = require('koa-router')();
let jade = require('jade');
let tmpl = jade.compileFile(__dirname + '/dashboard.jade')();
dashboard.get('/admin', function * () {
    if (!this.session.auth) {
        this.redirect('/admin/signin');
        return;
    }
    this.body = tmpl;
});

module.exports = dashboard;