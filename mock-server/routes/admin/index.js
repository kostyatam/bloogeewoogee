'use strict'
const Router = require('koa-router');
let admin = new Router;

let dashboard = require('./dashboard/dashboard.js');
let signin = require('./signin/signin.js');
admin
    .use(dashboard.routes(), dashboard.allowedMethods())
    .use(signin.routes(), signin.allowedMethods());

module.exports = admin;